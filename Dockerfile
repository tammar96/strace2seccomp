FROM archlinux:latest

WORKDIR /usr/src/app

COPY . .

RUN pacman -Sy --noconfirm gcc gcc-libs fmt pegtl make catch2 

RUN make clean

RUN make

RUN make check


